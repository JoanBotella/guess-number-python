
import random

MIN = 1
MAX = 100

secret = random.randrange(MIN, MAX + 1)

turn = 1

print("Try to guess my number! Is between " + str(MIN) + " and " + str(MAX))

run = True

while (run):
	guess = int(input("\nGuess #" + str(turn) + ": "))

	if (secret == guess):
		print("Yes! It took you " + str(turn) + " turn(s) to guess my number.")
		break

	elif (secret < guess):
		print("No! My number is smaller.")

	else:
		print("No! My number is bigger.")

	turn += 1
